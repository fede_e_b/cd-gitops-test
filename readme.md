## Imperative deployment and service for quick checks


```
### START copying everything here

# Create deploy and service and check they were created as intended
kubectl create deploy nginx-deploy --image=nginx:stable-alpine --replicas=1
kubectl expose deploy nginx-deploy --name nginx-svc --type=NodePort --port 80
kubectl get po,deploy,svc -o wide

# Create string with hostname.domain:NodePort and check it
NGINXSVCNODEPORT=$(kubectl get svc nginx-svc -o=jsonpath='{.spec.ports[?(@.port==80)].nodePort}')
echo "Checking http://$(hostname).$(dnsdomainname):$NGINXSVCNODEPORT/";sleep 1
curl $(hostname).$(dnsdomainname):$NGINXSVCNODEPORT

### STOP copying everything here and run it 
```
```
# Cleanup after successful tests
kubectl delete service/nginx-svc deploy/nginx-deploy
```